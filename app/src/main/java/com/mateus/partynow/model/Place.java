package com.mateus.partynow.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mateus on 12/03/17.
 */

public class Place {
    @JsonProperty(value = "id")
    private String id;
    @JsonProperty(value = "name")
    private String name;
    @JsonProperty(value = "picture")
    private Picture picture;
    @JsonProperty(value = "location")
    private Location location;

    public Place() {
        this.id = "";
        this.name = "";
        this.picture = new Picture();
        this.location = new Location();
    }

    public Place(String id, String name, Picture picture, Location location) {
        this.id = id;
        this.name = name;
        this.picture = picture;
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
