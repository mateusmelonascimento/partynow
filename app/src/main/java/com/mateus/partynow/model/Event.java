package com.mateus.partynow.model;

import android.graphics.Bitmap;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by root on 16/11/17.
 */

public class Event {
    @JsonProperty(value = "id")
    private String id;
    @JsonProperty(value = "name")
    private String name;
    @JsonProperty(value = "start_time")
    private String startTime;
    @JsonProperty(value = "end_time")
    private String endTime;
    @JsonProperty(value = "description")
    private String description;
    @JsonProperty(value = "cover")
    private Cover cover;

    private Bitmap coverBitmap;

    public Event() {
        this.id = "";
        this.name = "";
        this.startTime = "";
        this.endTime = "";
        this.description = "";
        this.cover = new Cover();
    }

    public Event(String id, String name, String startTime, String endTime, String description, Cover cover) {
        this.id = id;
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.description = description;
        this.cover = cover;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Cover getCover() {
        return cover;
    }

    public void setCover(Cover cover) {
        this.cover = cover;
    }

    public Bitmap getCoverBitmap() {
        return coverBitmap;
    }

    public void setCoverBitmap(Bitmap coverBitmap) {
        this.coverBitmap = coverBitmap;
    }
}
