package com.mateus.partynow.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by root on 16/11/17.
 */

public class PictureData {
    @JsonProperty(value = "url")
    private String url;

    public PictureData() {
        this.url = "";
    }

    public PictureData(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}