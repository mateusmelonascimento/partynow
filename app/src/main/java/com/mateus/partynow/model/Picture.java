package com.mateus.partynow.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by root on 16/11/17.
 */

public class Picture {
    @JsonProperty(value = "data")
    private PictureData pictureData;

    public Picture() {
        this.pictureData = new PictureData();
    }

    public Picture(PictureData pictureData) {
        this.pictureData = pictureData;
    }

    public PictureData getPictureData() {
        return pictureData;
    }

    public void setPictureData(PictureData pictureData) {
        this.pictureData = pictureData;
    }
}