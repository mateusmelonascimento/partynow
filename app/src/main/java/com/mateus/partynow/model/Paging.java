package com.mateus.partynow.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by root on 16/11/17.
 */

public class Paging {
    @JsonProperty(value = "previous")
    private String previous;
    @JsonProperty(value = "next")
    private String next;

    public Paging() {
        this.previous = "";
        this.next = "";
    }

    public Paging(String previous, String next) {
        this.previous = previous;
        this.next = next;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }
}
