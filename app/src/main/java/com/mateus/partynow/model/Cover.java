package com.mateus.partynow.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by root on 17/11/17.
 */

public class Cover {
    @JsonProperty(value = "source")
    private String source;

    public Cover() {
        this.source = "";
    }

    public Cover(String source) {
        this.source = source;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
