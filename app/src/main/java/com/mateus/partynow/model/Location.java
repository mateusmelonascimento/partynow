package com.mateus.partynow.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by mateus on 12/03/17.
 */

public class Location {
    @JsonProperty(value = "latitude")
    private double latitude;
    @JsonProperty(value = "longitude")
    private double longitude;
    @JsonProperty(value = "city")
    private String city;
    @JsonProperty(value = "state")
    private String state;
    @JsonProperty(value = "country")
    private String country;
    @JsonProperty(value = "street")
    private String street;
    @JsonProperty(value = "zip")
    private String zip;

    public Location() {
        this.latitude = 0;
        this.longitude = 0;
        this.city = "";
        this.state = "";
        this.country = "";
        this.street = "";
        this.zip = "";
    }

    public Location(double latitude, double longitude, String city, String state, String country, String street, String zip, String locatedIn) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.city = city;
        this.state = state;
        this.country = country;
        this.street = street;
        this.zip = zip;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }
}
