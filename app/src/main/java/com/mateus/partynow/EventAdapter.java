package com.mateus.partynow;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mateus.partynow.model.Event;

import java.util.ArrayList;

/**
 * Created by root on 16/11/17.
 */

public class EventAdapter extends ArrayAdapter<Event> {
    public EventAdapter(@NonNull Context context, @LayoutRes int resource, ArrayList<Event> events) {
        super(context, resource, events);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.fragment_event, parent, false);
        }

        Event event = getItem(position);
        if (event != null) {
            Bitmap cover = event.getCoverBitmap();
            if (cover != null) {
                ImageView iVCover = (ImageView) convertView.findViewById(R.id.iVCover);
                iVCover.setImageBitmap(cover);
            }

            String name = event.getName();
            if (name != null && !name.isEmpty()) {
                TextView tVName = (TextView) convertView.findViewById(R.id.tVName);
                tVName.setText(name);
            }

            String startTime = event.getStartTime();
            if (startTime != null && !startTime.isEmpty()) {
                TextView tVStartTime = (TextView) convertView.findViewById(R.id.tVStartTime);
                tVStartTime.setText("Começa: "+startTime);
            }

            String endTime = event.getEndTime();
            if (endTime != null && !endTime.isEmpty()) {
                TextView tVEndTime = (TextView) convertView.findViewById(R.id.tVEndTime);
                tVEndTime.setText("Termina: "+endTime);
            }

            String description = event.getDescription();
            if (description != null && !description.isEmpty()) {
                TextView tVDescription = (TextView) convertView.findViewById(R.id.tVDescription);
                tVDescription.setText(description);
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getContext(), "Não é possível acessar esta opção enquanto o servidor está fora do ar. Tente mais tarde.", Toast.LENGTH_LONG).show();
                }
            });
        }
        return convertView;
    }
}
