package com.mateus.partynow.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.io.IOUtils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by mateus on 23/04/17.
 */

public class Request {
    private final String URL_SERVER = "http://187.79.87.128:34340/";
    private final String GET_OBJECTS = "get-devices-last-location";
    private final int TIMEOUT = 5 * 1000;

    public synchronized ArrayList<Object> getObjects(String deviceId) {
        String spec =  URL_SERVER+GET_OBJECTS+"?deviceId="+deviceId;

        HttpURLConnection httpURLConnection = null;
        InputStream inputStream = null;
        try {
            URL url = new URL(spec);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setConnectTimeout(TIMEOUT);
            httpURLConnection.setReadTimeout(TIMEOUT);

            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                ObjectMapper mapper = new ObjectMapper();
                return mapper.readValue(IOUtils.toString(inputStream, "UTF-8"), new TypeReference<ArrayList<Object>>(){});
            }
        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            if (httpURLConnection != null) {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                httpURLConnection.disconnect();
            }
        }
        return new ArrayList<>();
    }
}