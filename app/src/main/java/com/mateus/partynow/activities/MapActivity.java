package com.mateus.partynow.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.mateus.partynow.EventAdapter;
import com.mateus.partynow.R;
import com.mateus.partynow.model.Event;
import com.mateus.partynow.util.Helper;
import com.mateus.partynow.util.broady.LatLngInterpolator;
import com.mateus.partynow.util.broady.MarkerAnimation;
import com.uber.sdk.android.core.UberSdk;
import com.uber.sdk.android.rides.RideParameters;
import com.uber.sdk.android.rides.RideRequestButton;
import com.uber.sdk.android.rides.RideRequestButtonCallback;
import com.uber.sdk.core.auth.Scope;
import com.uber.sdk.rides.client.ServerTokenSession;
import com.uber.sdk.rides.client.SessionConfiguration;
import com.uber.sdk.rides.client.error.ApiError;

import org.json.JSONException;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

public class MapActivity extends FragmentActivity implements
        OnMapReadyCallback,
        GoogleMap.OnInfoWindowClickListener,
        GoogleMap.OnInfoWindowLongClickListener,
        SensorEventListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        ResultCallback<LocationSettingsResult>,
        GoogleMap.OnMapLongClickListener {
    protected GoogleMap mMap;
    protected Marker mMarker;
    private Marker uberMarker;
    private Circle mCircle;
    private SensorManager mSensorManager;
    private Sensor mAccelerometerSensor;
    private Sensor mMagneticfieldSensor;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    protected Location mCurrentLocation;
    private FloatingActionButton mFAB;
    private Boolean mRequestingLocationUpdates;
    private final float[] mAccelerometerReading = new float[3];
    private final float[] mMagnetometerReading = new float[3];
    private final float[] mRotationMatrix = new float[9];
    private final float[] mOrientationAngles = new float[3];
    private float mDeclination;
    private final int REQUEST_CHECK_SETTINGS = 0x1;
    private final long UPDATE_INTERVAL_IN_MILLISECONDS = 0; // 0 second
//    private final long UPDATE_FASTEST_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS/2;
    private final long ZOOM = 18;
    private final String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    private final String KEY_LOCATION = "location";
    private boolean mFABClickedTwice;
    private ProgressDialog progressDialog;
    private static final String EVENTS_QUERY = "/events?fields=id,name,start_time,end_time,description,cover";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_maps);
        setContentView(R.layout.activity_main);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mFAB = (FloatingActionButton) findViewById(R.id.fab);
        mFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startUpdatesButtonHandler();
            }
        });

        // Get sensors
        updateSensor();

        mRequestingLocationUpdates = false;

        // Update values using data stored in the Bundle.
        updateValuesFromBundle(savedInstanceState);

        // Kick off the process of building the GoogleApiClient, LocationRequest, and
        // LocationSettingsRequest objects.
        buildGoogleApiClient();
        createLocationRequest();
        buildLocationSettingsRequest();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setOnInfoWindowClickListener(this);
        mMap.setOnInfoWindowLongClickListener(this);
        mMap.setOnMapLongClickListener(this);

        updateLocationUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Get updates from the accelerometer and magnetometer at a constant rate.
        // To make batch operations more efficient and reduce power consumption,
        // provide support for delaying updates to the application.
        //
        // In this example, the sensor reporting delay is small enough such that
        // the application receives an update before the system checks the sensor
        // readings again.
        mSensorManager.registerListener(this, mAccelerometerSensor, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mMagneticfieldSensor, SensorManager.SENSOR_DELAY_NORMAL);

        if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);

        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        updateCamera(marker.getPosition());
    }

    @Override
    public void onInfoWindowLongClick(Marker marker) {
        if (!marker.getId().equals(mMarker.getId()) && marker.getTag() instanceof com.mateus.partynow.model.Place) {
            com.mateus.partynow.model.Place place = (com.mateus.partynow.model.Place) marker.getTag();
            loadEvents(place.getId()+EVENTS_QUERY);
        } else if (marker.getTitle().equals(getString(R.string.marker_tittle_uber))) {
            if (mCurrentLocation != null) {
                requestRide(mCurrentLocation, marker.getPosition());
            }
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (mMap != null && (event.values[0] != mAccelerometerReading[0] || event.values[0] != mMagnetometerReading[0])) {
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                System.arraycopy(event.values, 0, mAccelerometerReading, 0, mAccelerometerReading.length);
            } else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
                System.arraycopy(event.values, 0, mMagnetometerReading, 0, mMagnetometerReading.length);
            }
            updateOrientationAngles();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (mCurrentLocation == null) {
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            updateLocationUI();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                startLocationUpdates();
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                try {
                    status.startResolutionForResult(MapActivity.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (isBetterLocation(location, mCurrentLocation)) {
            mCurrentLocation = location;
            updateLocationUI();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        break;
                }
                break;
        }
    }

    private void updateSensor() {
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometerSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mMagneticfieldSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        if (mAccelerometerSensor == null || mMagneticfieldSensor == null) {
            Toast.makeText(this, R.string.sorry_you_cannot_run_this_application, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean(KEY_REQUESTING_LOCATION_UPDATES, mRequestingLocationUpdates);
        savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation);
        super.onSaveInstanceState(savedInstanceState);
    }

    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            // Update the value of mRequestingLocationUpdates from the Bundle, and make sure that
            // the Start Updates and Stop Updates buttons are correctly enabled or disabled.
            if (savedInstanceState.keySet().contains(KEY_REQUESTING_LOCATION_UPDATES)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(KEY_REQUESTING_LOCATION_UPDATES);
            }

            // Update the value of mCurrentLocation from the Bundle and update the UI to show the
            // correct latitude and longitude.
            if (savedInstanceState.keySet().contains(KEY_LOCATION)) {
                // Since KEY_LOCATION was found in the Bundle, we can be sure that mCurrentLocation
                // is not null.
                mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            }
            updateUI();
        }
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        //mLocationRequest.setFastestInterval(UPDATE_FASTEST_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    private void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }

    private boolean verifyAndRequestPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            return false;
        }
        return true;
    }

    private void startUpdatesButtonHandler() {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M || verifyAndRequestPermissions()) {
            checkLocationSettings();
        }
    }

    private void stopUpdatesButtonHandler() {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        stopLocationUpdates();
    }

    private void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = true;
                setButtonsEnabledState();
            }
        });

    }

    private void updateUI() {
        setButtonsEnabledState();
        updateLocationUI();
    }

    private void updateCircleUI(LatLng latLng, float accuracy) {
        if (mCircle == null) {
            mCircle = mMap.addCircle(new CircleOptions()
                    .center(latLng)
                    .radius(accuracy)
                    .strokeWidth(1)
                    .strokeColor(R.color.strokeColor)
                    .fillColor(R.color.fillColor));
        } else {
            mCircle.setCenter(latLng);
            mCircle.setRadius(accuracy);
        }
    }

    private void updateDeclination(Location location) {
        GeomagneticField field = new GeomagneticField(
                (float)location.getLatitude(),
                (float)location.getLongitude(),
                (float)location.getAltitude(),
                System.currentTimeMillis()
        );

        // getDeclination returns degrees
        mDeclination = field.getDeclination();
    }

    protected void updateLocationUI() {
        if (mMap != null && mCurrentLocation != null) {
            updateDeclination(mCurrentLocation);

            LatLng latLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());

            if (mMarker != null) {
                MarkerAnimation.animateMarkerToGB(mMarker, latLng, new LatLngInterpolator.Spherical());

            } else {
                mMarker = mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(getString(R.string.you))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_icon_64_px)));
                updateCamera(latLng);
            }
            mMarker.setSnippet(dateMillisToString(mCurrentLocation.getTime()));
            updateCircleUI(latLng, mCurrentLocation.getAccuracy());
            if (mFABClickedTwice) {
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            }
        }
    }

    private void updateCamera(LatLng latLng) {
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(ZOOM));
    }

    public void updateOrientationAngles() {
        // Update rotation matrix, which is needed to update orientation angles.
        boolean rotationMatrix = SensorManager.getRotationMatrix(mRotationMatrix, null, mAccelerometerReading, mMagnetometerReading);
        // "mRotationMatrix" now has up-to-date information.

        if (rotationMatrix) {
            float xOldAngle = (float) Math.toDegrees(mOrientationAngles[0]) + mDeclination;
            SensorManager.getOrientation(mRotationMatrix, mOrientationAngles);
            // "mOrientationAngles" now has up-to-date information.
            float xCurrentAngle = (float) Math.toDegrees(mOrientationAngles[0]) + mDeclination;

            if (Math.abs(xOldAngle - xCurrentAngle) > 4) {
                if (!mFABClickedTwice) {
                    if (mMarker != null) {
                        lookAtDirection(xCurrentAngle, true);
                    }
                } else {
                    lookAtDirection(xCurrentAngle, false);
                }
            }
        }
    }

    private void lookAtDirection(float bearing, boolean flat) {
        mMarker.setFlat(flat);
        if (flat) {
            mMarker.setRotation(bearing);
        } else {
            mMarker.setRotation(0);
            updateCameraPosition(30, bearing, 50);
        }
    }

    private void updateCameraPosition(float tilt, float bearing, int animationInMillis) {
        CameraPosition cameraPosition = CameraPosition
                .builder(mMap.getCameraPosition())
                .tilt(tilt)
                .bearing(bearing)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), animationInMillis, null);
    }

    private void setButtonsEnabledState() {
        if (mRequestingLocationUpdates) {
            mFAB.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.fab_update1)));
            mFAB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mFABClickedTwice) {
                        updateCameraPosition(0, mMarker.getRotation(), 200);
                        stopUpdatesButtonHandler();
                    } else {
                        mFAB.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(MapActivity.this, R.color.fab_update2)));
                    }
                    mFABClickedTwice = !mFABClickedTwice;
                }
            });

        } else {
            mFAB.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, R.color.gray)));
            mFAB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startUpdatesButtonHandler();
                }
            });
        }
    }

    private void stopLocationUpdates() {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that request frequent location updates.
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                mRequestingLocationUpdates = false;
                setButtonsEnabledState();
            }
        });
    }

    private boolean isBetterLocation(Location newLocation, Location oldLocation) {
        if (oldLocation == null) {
            return true;
        }

        long timeDelta = newLocation.getTime() - oldLocation.getTime();
        boolean isNewer = timeDelta > 0;

        if (!isNewer) {
            return false;
        }

        if (newLocation.getAccuracy() > 100) {
            return false;
        }

        int accuracyDelta = (int) (newLocation.getAccuracy() - oldLocation.getAccuracy());

        return accuracyDelta <= 0 || isSameProvider(newLocation.getProvider(), oldLocation.getProvider());
    }

    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    private String dateMillisToString(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calendar.getTime());
    }

    private void showInformationDialog(ArrayAdapter<Event> eventsAdapter) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_event, null);

        ListView lVevents = (ListView) dialogView.findViewById(R.id.lVEvents);
        lVevents.setAdapter(eventsAdapter);

        builder.setView(dialogView);
        builder.create().show();
        progressDialog.dismiss();
    }

    private void loadEvents(String query) {
        progressDialog = ProgressDialog.show(this, null, getResources().getString(R.string.loading_events), true, false);
        GraphRequest graphRequest = new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                query,
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        handleResponse(response);

                    }
                }
        );
        graphRequest.executeAsync();
    }

    private void handleResponse(GraphResponse response) {
        try {
            if (response != null) {
                String data = response.getJSONObject().getString("data");
                Log.i("updateUI", data);
                String pagingString = response.getJSONObject().getString("paging");
                Log.i("updateUI", pagingString);

                ObjectMapper mapper = new ObjectMapper();
                mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                ArrayList<Event> events = mapper.readValue(data, new TypeReference<ArrayList<Event>>() {});

                if (events.size() > 0) {
                    new EventPictureGetter(events).execute();
                    return;
                }
            }
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        progressDialog.dismiss();
        Toast.makeText(this, R.string.there_is_no_event_for_this_place, Toast.LENGTH_LONG).show();
    }

    private class EventPictureGetter extends AsyncTask<Void, Void, ArrayList<Event>> {
        private ArrayList<Event> events;

        private EventPictureGetter(ArrayList<Event> events) {
            this.events = events;
        }

        @Override
        protected ArrayList<Event> doInBackground(Void... voids) {
            for (int i=0; i<events.size(); i++) {
                events.get(i).setCoverBitmap(Helper.getBitmapFromURL(events.get(i).getCover().getSource()));
            }
            return events;
        }

        @Override
        protected void onPostExecute(ArrayList<Event> events) {
            super.onPostExecute(events);
            EventAdapter eventsAdapter = new EventAdapter(MapActivity.this, R.layout.fragment_event, events);
            showInformationDialog(eventsAdapter);
        }
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        if (uberMarker != null) {
            uberMarker.remove();
        }
        uberMarker = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title(getString(R.string.marker_tittle_uber))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.black_car)));
    }

    private void requestRide(Location pickUpLocation, LatLng dropOffLocation) {
        SessionConfiguration config = new SessionConfiguration.Builder()
                // mandatory
                .setClientId("<CLIENT_ID>")
                // required for enhanced button features
                .setServerToken("<TOKEN>")
                // required for implicit grant authentication
                .setRedirectUri("<REDIRECT_URI>")
                // required scope for Ride Request Widget features
                .setScopes(Arrays.asList(Scope.RIDE_WIDGETS))
                // optional: set sandbox as operating environment
                .setEnvironment(SessionConfiguration.Environment.SANDBOX)
                .build();
        UberSdk.initialize(config);


        RideParameters rideParams = new RideParameters.Builder()
                // Optional product_id from /v1/products endpoint (e.g. UberX). If not provided, most cost-efficient product will be used
                .setProductId("a1111c8c-c720-46c3-8534-2fcdd730040d")
                // Required for price estimates; lat (Double), lng (Double), nickname (String), formatted address (String) of dropoff location
                // Required for pickup estimates; lat (Double), lng (Double), nickname (String), formatted address (String) of pickup location
                .setPickupLocation(pickUpLocation.getLatitude(), pickUpLocation.getLongitude(), "Minha localização", "")
                // Required for price estimates; lat (Double), lng (Double), nickname (String), formatted address (String) of dropoff location.
                .setDropoffLocation(dropOffLocation.latitude, dropOffLocation.longitude, "Meu destino", "")
                .build();

        RideRequestButton rideRequestButton = (RideRequestButton) findViewById(R.id.rideRequestButton);
        rideRequestButton.setRideParameters(rideParams);

        ServerTokenSession session = new ServerTokenSession(config);
        rideRequestButton.setSession(session);

        rideRequestButton.loadRideInformation();

        RideRequestButtonCallback callback = new RideRequestButtonCallback() {

            @Override
            public void onRideInformationLoaded() {
                Log.i("UBER", "onRideInformationLoaded");
                // react to the displayed estimates
            }

            @Override
            public void onError(ApiError apiError) {
                Log.i("UBER", "onError");
                // API error details: /docs/riders/references/api#section-errors
            }

            @Override
            public void onError(Throwable throwable) {
                Log.i("UBER", "onError");
                // Unexpected error, very likely an IOException
            }
        };
        rideRequestButton.setCallback(callback);
        rideRequestButton.performClick();
    }
}
