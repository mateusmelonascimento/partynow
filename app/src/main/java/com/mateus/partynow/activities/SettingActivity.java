package com.mateus.partynow.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.mateus.partynow.R;

/**
 * Created by root on 18/11/17.
 */

public class SettingActivity extends AppCompatActivity {
    public static final int SETTING_ACTIVITY_CODE = 1010;
    public static final int RADIUS_DEFAULT = 1000; // in metters
    public static int RADIUS = 1000; // in metters
    public static final String PLACE_TYPE_DEFAULT = "TODOS";
    public static String PLACE_TYPE = "TODOS";

    private EditText mETRadius;
    private Spinner mSPlaceType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_configuration);
        mETRadius = (EditText) findViewById(R.id.eTRadius);
        mSPlaceType = (Spinner) findViewById(R.id.sPlaceType);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUI();
    }

    public void onSaveClick(View view) {
        String radius = mETRadius.getText().toString();
        if (radius.isEmpty() || radius.equals("0")) {
            Toast.makeText(this, R.string.put_a_valid_number, Toast.LENGTH_LONG).show();
        } else {
            RADIUS = Integer.valueOf(radius);
            PLACE_TYPE = getPlaceType();
            Toast.makeText(this, R.string.saved, Toast.LENGTH_LONG).show();
            updateUI();
        }
    }

    public void onResetClick(View view) {
        RADIUS = RADIUS_DEFAULT;
        PLACE_TYPE = PLACE_TYPE_DEFAULT;
        Toast.makeText(this, R.string.reseted, Toast.LENGTH_LONG).show();
        updateUI();
    }

    public void onGoBackClick(View view) {
        onBackPressed();
    }

    private void updateUI() {
        mETRadius.setText(String.valueOf(RADIUS));
        setPlaceType();
    }
    
    private void setPlaceType() {
        String[] placeTypes = getResources().getStringArray(R.array.place_type);
        for (int i=0; i<placeTypes.length; i++) {
            if (placeTypes[i].equals(PLACE_TYPE)) {
                mSPlaceType.setSelection(i);
                break;
            }
        }
    }
    
    private String getPlaceType() {
        return mSPlaceType.getSelectedItem().toString();
    }
}
