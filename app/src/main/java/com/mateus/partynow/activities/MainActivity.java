package com.mateus.partynow.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mateus.partynow.R;
import com.mateus.partynow.model.Place;
import com.mateus.partynow.util.Helper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends MapActivity implements NavigationView.OnNavigationItemSelectedListener {
    private LoginButton mLoginButton;
    private CallbackManager mCallbackManager;
    private NavigationView mNavigationView;

    private String getPlaceQuery() {
        double lat = -3.71839;
        double lng = -38.5434;
        if (mCurrentLocation != null) {
            lat = mCurrentLocation.getLatitude();
            lng = mCurrentLocation.getLongitude();
        }
        String[] placeTypes = getResources().getStringArray(R.array.place_type);
        if (SettingActivity.PLACE_TYPE.equals(placeTypes[0])) {
            return "/search?type=place&center="+lat+","+lng+"&distance="+SettingActivity.RADIUS+"&fields=name,picture,location";
        }
        return "/search?type=place&categories=[\""+SettingActivity.PLACE_TYPE+"\"]&center="+lat+","+lng+"&distance="+SettingActivity.RADIUS+"&fields=name,picture,location";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);
        loadFBResources();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateFacebookMenuTitle(isUserLoggedIn());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        super.onMapReady(googleMap);
        if (isUserLoggedIn()) {
            loadPlaces(getPlaceQuery());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SettingActivity.SETTING_ACTIVITY_CODE) {
            if (mMap != null && isUserLoggedIn()) {
                mMap.clear();
                mMarker = null;
                updateLocationUI();
                loadPlaces(getPlaceQuery());
            }
        }
    }

    private void loadFBResources() {
        // Other app specific specialization
        mCallbackManager = CallbackManager.Factory.create();

        // Callback registration
        mLoginButton = (LoginButton) findViewById(R.id.login_button);
        mLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.i("onSuccess", loginResult.toString());
                loadPlaces(getPlaceQuery());
            }

            @Override
            public void onCancel() {
                Log.i("onCancel", "");
            }

            @Override
            public void onError(FacebookException exception) {
                Log.i("onError", exception.toString());
            }
        });
    }

    private void loadPlaces(String query) {
        GraphRequest graphRequest = new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                query,
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        try {
                            handleResponse(response);
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
        );
        graphRequest.executeAsync();
    }

    private void handleResponse(GraphResponse response) throws JSONException, IOException {
        if (response != null) {
            JSONObject jsonObject = response.getJSONObject();
            if (jsonObject != null) {
                String data = jsonObject.getString("data");
                Log.i("updateUI", data);

                ObjectMapper mapper = new ObjectMapper();
                mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                ArrayList<Place> places = mapper.readValue(data, new TypeReference<ArrayList<Place>>() {});

                for (Place place : places) {
                    new MapUIUpdater(place).execute();
                }
            }
        } else {
            Log.i("updateUI", "null response");
        }
    }

    private boolean isUserLoggedIn() {
        return AccessToken.getCurrentAccessToken() != null;
    }

    private class MapUIUpdater extends AsyncTask<Void, Void, Bitmap> {
        private Place place = null;

        private MapUIUpdater(Place place) {
            this.place = place;
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            String src = place.getPicture().getPictureData().getUrl();
            if (src == null || src.isEmpty()) {
                return null;
            }
            return Helper.getBitmapFromURL(src);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            updateMapUI(bitmap);
        }

        private void updateMapUI(Bitmap bitmap) {
            if (mMap != null) {
                LatLng latLng = new LatLng(place.getLocation().getLatitude(), place.getLocation().getLongitude());
                mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(place.getName())
                        .icon(bitmap == null ? BitmapDescriptorFactory.fromResource(R.drawable.place) : BitmapDescriptorFactory.fromBitmap(bitmap))
                        .snippet(getString(R.string.marker_snippet)))
                        .setTag(place);

            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            Toast.makeText(this, "Não é possível acessar esta opção enquanto o servidor está fora do ar. Tente mais tarde.", Toast.LENGTH_LONG).show();
        } else if (id == R.id.nav_share) {
            Toast.makeText(this, "Funcionalidade não desenvolvida.", Toast.LENGTH_LONG).show();
        } else if (id == R.id.nav_setting) {
            startActivityForResult(new Intent(this, SettingActivity.class), SettingActivity.SETTING_ACTIVITY_CODE);
        } else if (id == R.id.nav_login_facebook) {
            mLoginButton.performClick();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void updateFacebookMenuTitle(boolean logged) {
        Menu menu = mNavigationView.getMenu();
        MenuItem nav_camara = menu.findItem(R.id.nav_login_facebook);
        nav_camara.setTitle(logged ? R.string.logout : R.string.login);
    }
}
